package pack;

public abstract class Filmografia {
    
    String titulo, genero;
    int año;
    
    public Filmografia(String titulo, String genero, int año){
        this.titulo = titulo;
        this.genero = genero;
        this.año = año;
    }
    public String getTitulo(){
        return titulo;
    }
    public String getGenero(){
        return genero;
    }
    public int getAño(){
        return año;
    }
}
