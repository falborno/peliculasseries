package pack;

import java.util.ArrayList;

public class Catalogo{
    
    ArrayList<Filmografia>  listado = new ArrayList();

    public static void main(String[] args){
        
        Catalogo catalogo = new Catalogo();
        
        catalogo.cargarCatalogo(catalogo.listado, new Pelicula("Top Secret!","Comedia",1984));
        catalogo.cargarCatalogo(catalogo.listado, new Pelicula("The Truman Show","Drama",1998));
        catalogo.cargarCatalogo(catalogo.listado, new Pelicula("Wind River","Drama",2017));
        catalogo.cargarCatalogo(catalogo.listado, new Pelicula("Avangers: Infinity War","Acción",2018));
        
        catalogo.cargarCatalogo(catalogo.listado, new Serie("Friends","Sitcom",1994,10));
        catalogo.cargarCatalogo(catalogo.listado, new Serie("Breaking Bad","Drama",2008,7));
        catalogo.cargarCatalogo(catalogo.listado, new Serie("Scrubs","Comedia",2001,6));
        
        catalogo.mostrarCatalogo(catalogo.listado);
        
    }
    
    void cargarCatalogo(ArrayList lista, Filmografia arg){
            lista.add(arg);
    
    }
    
    void mostrarCatalogo(ArrayList arg){

        for(int i = 0; i < arg.size(); i++){

            System.out.println(arg.get(i).toString());   
        }
    }
}